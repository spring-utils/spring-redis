package springredis.controller;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.flogger.Flogger;
import springredis.resources.Book;
import springredis.resources.Library;
import springredis.service.BookService;
import springredis.service.LibraryService;
import springredis.service.RedisService;

@Flogger
@RestController
@RequestMapping("/library")
public class LibraryController {

	@Autowired
	private RedisService redisService;

	@Autowired
	private LibraryService libraryService;

	@Autowired
	private BookService bookService;

	@GetMapping
	public ResponseEntity<Collection<Library>> getLibraries() {
		log.atInfo().log("Get all libraries!");
		return ResponseEntity.ok(redisService.getAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Library> getLibrary(@PathVariable("id") String id) {
		log.atInfo().log("Get library: " + id);
		return ResponseEntity.ok(redisService.find(id));
	}

	@PostMapping("/{id}")
	public ResponseEntity<Library> createLibrary(@PathVariable("id") String id) {
		Library library = redisService.find(id);
		if (library != null) {
			log.atInfo().log("Found library: " + id);
			return ResponseEntity.ok(library);
		}

		log.atInfo().log("Create library: " + id);
		Book book = bookService.create(LocalDateTime.now().toString(), 100);
		library = libraryService.create(id, Arrays.asList(book));
		redisService.save(library);
		return ResponseEntity.ok(library);
	}

	@PostMapping("/many/{amount}")
	public ResponseEntity<String> createLotOfLibrary(@PathVariable("amount") int amount) {
		for (int i = 0; i < amount; i++) {
			String id = LocalDateTime.now().toString();
			createLibrary(id);
		}

		return ResponseEntity.ok(amount + "");
	}
}
