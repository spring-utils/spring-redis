package springredis.repository;

import static java.util.stream.Collectors.toSet;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;

import springredis.resources.Library;
import springredis.service.JsonSerializer;

@Repository
public class RedisRepository {
	private static final String KEY = "Library/";

	@Resource(name = "redisTemplate")
	private HashOperations<String, String, String> hashOperations;

	public void add(Library library) {
		String json = JsonSerializer.toJson(library);
		hashOperations.put(KEY, library.getId(), json);
	}

	public Library find(String id) {
		String result = hashOperations.get(KEY, id);
		if (result == null) {
			return null;
		}
		return JsonSerializer.fromJson(result, Library.class);
	}

	public Collection<Library> getAll() {
		Collection<String> collection = hashOperations.entries(KEY).values();
		return collection.stream().map(str -> JsonSerializer.fromJson(str, Library.class)).collect(toSet());
	}
}
