package springredis.service;

import org.springframework.stereotype.Service;

import springredis.resources.Book;

@Service
public class BookService {

	public Book create(String id, int pages) {
		return new Book(id, pages);
	}
}
