package springredis.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springredis.repository.RedisRepository;
import springredis.resources.Library;

@Service
public class RedisService {

	@Autowired
	private RedisRepository repository;

	public void save(Library library) {
		repository.add(library);
	}

	public Library find(String id) {
		return repository.find(id);
	}

	public Collection<Library> getAll() {
		return repository.getAll();
	}
}
