package springredis.service;

import java.util.List;

import org.springframework.stereotype.Service;

import springredis.resources.Book;
import springredis.resources.Library;

@Service
public class LibraryService {

	public Library create(String id, List<Book> books) {
		return new Library(id, books);
	}

}
