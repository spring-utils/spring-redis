package springredis.service;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;

import springredis.resources.Book;
import springredis.resources.Library;
import springredis.service.BookService;
import springredis.service.LibraryService;
import springredis.service.RedisService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisServiceTest {

	@ClassRule
	public static GenericContainer<?> redisContainer = new GenericContainer<>("redis:5.0.3-alpine")
			.withExposedPorts(6379);

	@Autowired
	private BookService bookService;

	@Autowired
	private LibraryService libraryService;

	@Autowired
	private RedisService redisService;

	@Test
	public void test() {
		Book book = bookService.create("1", 386);
		Library library = libraryService.create("1", asList(book));

		redisService.save(library);

		Library findLibrary = redisService.find("1");

		assertThat(library).isEqualTo(findLibrary);
	}

	@TestConfiguration
	static class TestConfig {

		@Bean
		public LettuceConnectionFactory lettuceConnectionFactory() {
			String address = redisContainer.getContainerIpAddress();
			Integer port = redisContainer.getFirstMappedPort();

			LettuceConnectionFactory factory = new LettuceConnectionFactory(address, port);
			factory.setShareNativeConnection(false);
			return factory;
		}
	}
}
